#ifndef TASK_H
#define TASK_H

#include <QObject>

class Task : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY priorityChanged)

public:
    explicit Task(QObject *parent = nullptr);
    Task(const QString &title, const QString &description, int priority, QObject *parent = nullptr);

    QString title() const;
    void setTitle(const QString &title);

    QString description() const;
    void setDescription(const QString &description);

    int priority() const;
    void setPriority(int priority);

    static const int LEAST_CRITICAL_PRIORITY = 3;

signals:
    void titleChanged();
    void descriptionChanged();
    void priorityChanged();

private:
    QString m_title;
    QString m_description;
    int m_priority;
};


#endif // TASK_H
