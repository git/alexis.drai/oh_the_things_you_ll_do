#ifndef OBSERVABLETASKLIST_H
#define OBSERVABLETASKLIST_H

#include <QAbstractListModel>
#include "Task.h"

class ObservableTaskList : public QAbstractListModel
{
    Q_OBJECT

public:
    enum TaskRoles {
        TitleRole,
        DescriptionRole,
        PriorityRole
    };

    explicit ObservableTaskList(QObject *parent = nullptr);

    void addTask(Task* task);
    void removeTask(int index);
    void updateTask(int index, Task* task);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::DisplayRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent= QModelIndex()) override;
    Q_INVOKABLE bool insertRows(int row, int count, const QModelIndex & parent = QModelIndex()) override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QList<Task*> m_tasks;
};

#endif // OBSERVABLETASKLIST_H
