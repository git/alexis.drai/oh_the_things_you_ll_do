#ifndef PROJECT_H
#define PROJECT_H

#include <QObject>
#include "ObservableTaskList.h"

class Project : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(ObservableTaskList* tasks READ tasks CONSTANT)

public:
    explicit Project(QObject *parent = nullptr);
    Project(const QString &title, ObservableTaskList* tasks, QObject *parent = nullptr);

    QString title() const;
    void setTitle(const QString &title);

    ObservableTaskList* tasks() const;

signals:
    void titleChanged();

private:
    QString m_title;
    ObservableTaskList* m_tasks;
};

#endif // PROJECT_H
