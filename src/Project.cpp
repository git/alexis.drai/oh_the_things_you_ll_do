#include "Project.h"

Project::Project(QObject *parent) : QObject(parent)
{
}

Project::Project(const QString &title, ObservableTaskList* tasks, QObject *parent)
    : QObject(parent), m_title(title), m_tasks(tasks)
{
}

QString Project::title() const
{
    return m_title;
}

void Project::setTitle(const QString &title)
{
    if (m_title != title) {
        m_title = title;
        emit titleChanged();
    }
}

ObservableTaskList* Project::tasks() const
{
    return m_tasks;
}
