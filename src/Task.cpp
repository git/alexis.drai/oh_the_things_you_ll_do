#include "Task.h"

Task::Task(QObject *parent) : QObject(parent)
{
}

Task::Task(const QString &title, const QString &description, int priority, QObject *parent)
    : QObject(parent), m_title(title), m_description(description), m_priority(priority)
{
}

QString Task::title() const
{
    return m_title;
}

void Task::setTitle(const QString &title)
{
    if (m_title != title) {
        m_title = title;
        emit titleChanged();
    }
}

QString Task::description() const
{
    return m_description;
}

void Task::setDescription(const QString &description)
{
    if (m_description != description) {
        m_description = description;
        emit descriptionChanged();
    }
}

int Task::priority() const
{
    return m_priority;
}

void Task::setPriority(int priority)
{
    if (priority >= 0 && priority <= LEAST_CRITICAL_PRIORITY && m_priority != priority) {
        m_priority = priority;
        emit priorityChanged();
    }
}

