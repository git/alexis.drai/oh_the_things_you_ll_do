#include "ObservableTaskList.h"

ObservableTaskList::ObservableTaskList(QObject *parent)
    : QAbstractListModel(parent)
{
}

void ObservableTaskList::addTask(Task* task)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_tasks << task;
    endInsertRows();
}

void ObservableTaskList::removeTask(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    m_tasks.removeAt(index);
    endRemoveRows();
}

void ObservableTaskList::updateTask(int index, Task* task)
{
    m_tasks[index] = task;
    emit dataChanged(this->index(index), this->index(index), {TitleRole, DescriptionRole, PriorityRole});
}

int ObservableTaskList::rowCount(const QModelIndex &) const
{
    return m_tasks.count();
}

QVariant ObservableTaskList::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_tasks.count())
        return QVariant();

    Task* task = m_tasks[index.row()];
    if (role == TitleRole)
        return task->title();
    else if (role == DescriptionRole)
        return task->description();
    else if (role == PriorityRole)
        return task->priority();

    return QVariant();
}

bool ObservableTaskList::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.row() < 0 || index.row() >= m_tasks.count())
        return false;

    if ( data(index, role) == value)
        return true;

    Task* task = m_tasks[index.row()];
    if (role == TitleRole)
        task->setTitle(value.toString());
    else if (role == DescriptionRole)
        task->setDescription(value.toString());
    else if (role == PriorityRole)
        task->setPriority(value.toInt());

    emit dataChanged(index, index, QVector<int>() << role);
    return true;
}

Qt::ItemFlags ObservableTaskList::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

bool ObservableTaskList::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    for (int nb = 0; nb < count; ++nb) {
        Task* newTask = new Task("no title", "no description", 3);
        m_tasks.insert(row, newTask);
    }
    endInsertRows();

    return true;
}


bool ObservableTaskList::removeRows(int row, int count, const QModelIndex &parent)
{
    if (row < 0 || row + count > m_tasks.count())
        return false;

    beginRemoveRows(parent, row, row + count - 1);
    for (int nb = 0; nb < count; ++nb) {
        m_tasks.removeAt(row);
    }
    endRemoveRows();

    return true;
}

QHash<int, QByteArray> ObservableTaskList::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TitleRole] = "title";
    roles[DescriptionRole] = "description";
    roles[PriorityRole] = "priority";
    return roles;
}
