
# Oh the things you'll do

- [Auteur](#auteur)
- [Descriptif](#descriptif)
- [Usage](#usage)
- [Techniques de programmation utilisées](#techniques-de-programmation-utilisées)
  - ["master" : SilicaListView](#master--silicalistview)
  - ["detail": champs modifiables avec validation](#detail--champs-modifiables-avec-validation)

## Auteur

- [Alexis Drai](https://codefirst.iut.uca.fr/git/alexis.drai)

## Descriptif

*Oh the things you'll do* est une app de choses à faire. Elle affiche une liste de `tasks` qui appartiennent à un `project`.

On peut faire des opérations CRUD sur les `tasks`.

Un projet a un titre et des `tasks`.

Une `task` a un titre, une description, et un niveau de priorité entre `0` et `3` (`0` étant le plus critique).

```mermaid
classDiagram
    class Project {
        +String title
    }
    
    class Task {
        +String title
        +String description
        +int priority
    }
    
    class ObservableTaskList {
        +addTask(Task*)
        +removeTask(int)
        +updateTask(int, Task*)
        +rowCount(QModelIndex) const
        +data(QModelIndex, int) const
        +setData(QModelIndex, QVariant, int)
        +flags(QModelIndex) const
        +removeRows(int, int, QModelIndex)
        +insertRows(int, int, QModelIndex)
        +roleNames() const
    }

    Project --> ObservableTaskList : tasks
    ObservableTaskList --> "*" Task : m_tasks
    QAbstractListModel <|-- ObservableTaskList

```

## Usage

Il suffit de lancer l'application, et vous pouvez tester les opérations CRUD sur les `tasks`.

Les boutons disponibles sont décrits [plus bas](#techniques-de-programmation-utilisées).

## Techniques de programmation utilisées

### "master" : SilicaListView
La liste présente sur la page principale est une `ObservableTaskList`, une redéfinition maison de 
`QAbstractListModel`. Ainsi, l'app peut observer la liste, et afficher tous les changements qu'on lui inflige.

On y voit:
* un bouton `Create` en haut de l'écran pour créer des `tasks`
* un bouton `Delete` en dessous de chaque `task` pour l'effacer
* un bouton `GoTo` en dessous de chaque `task` pour accéder à ses détails

<img src="./docs/main.png" width="410" style="margin:20px" alt="">

### "detail" : champs modifiables avec validation
Le titre et la description de la `task` sont éditables. C'est aussi le cas pour la priorité -- et l'utilisateur ne peut entrer qu'un entier entre `0` (le plus critique) et `3` (le moins critique).

<img src="./docs/detail.png" width="410" style="margin:20px" alt="">
