# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = oh_the_things_you_ll_do

CONFIG += sailfishapp

SOURCES += src/oh_the_things_you_ll_do.cpp \
    src/ObservableTaskList.cpp \
    src/Project.cpp \
    src/Task.cpp

DISTFILES += qml/oh_the_things_you_ll_do.qml \
    qml/cover/CoverPage.qml \
    qml/pages/ProjectView.qml \
    qml/pages/TaskView.qml \
    rpm/oh_the_things_you_ll_do.changes.in \
    rpm/oh_the_things_you_ll_do.changes.run.in \
    rpm/oh_the_things_you_ll_do.spec \
    translations/*.ts \
    oh_the_things_you_ll_do.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/oh_the_things_you_ll_do-de.ts

HEADERS += \
    src/ObservableTaskList.h \
    src/Project.h \
    src/Task.h
