import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"
import fr.uca.iut 1.0

ApplicationWindow {
    id: window
    initialPage: Component {
        ProjectView {
            id: projectView
        }
    }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations
}

