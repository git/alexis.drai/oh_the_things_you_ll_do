import QtQuick 2.0
import Sailfish.Silica 1.0
import fr.uca.iut 1.0

Page {
    id: projectView

    SilicaFlickable {
        anchors.fill: parent

        TextField {
            id: titleField
            text: projectModel.title
            label: qsTr("Project Title")
            font.pixelSize: Theme.fontSizeExtraLarge
            onTextChanged: projectModel.title = text
            anchors {
                top: parent.top
                left: parent.left
            }
        }

        Button {
            id: addButton
            text: "Create"
            onClicked: {
                projectModel.tasks.insertRows(0, 1);
                // FIXME this does insert a new task in the list, but the task title doesn't get displayed
            }
            anchors {
                top: titleField.bottom
                left: parent.left
            }
        }

        SilicaListView {
            id: tasksList
            model: projectModel.tasks
            anchors {
                top: addButton.bottom
                left: parent.left
                bottom: parent.bottom
            }
            delegate: ListItem {
                width: ListView.view.width
                height: Theme.itemSizeLarge

                Label {
                    id: label
                    text: model.title
                    color: model.priority === 0 ? "red" :
                           model.priority === 1 ? "orange" :
                           model.priority === 2 ? "blue" : Theme.primaryColor
                    font.pixelSize: Theme.fontSizeLarge
                }

                Button {
                    id: navButton
                    anchors {
                        top: label.bottom
                        left: parent.left
                    }
                    height: Theme.itemSizeSmall
                    text: "GoTo"
                    onClicked: pageStack.push(Qt.resolvedUrl("TaskView.qml"), {taskModel: model})
                }

                Button {
                    id: deleteButton
                    anchors {
                        top: label.bottom
                        left: navButton.right
                    }
                    height: Theme.itemSizeSmall
                    text: "Delete"
                    onClicked: projectModel.tasks.removeRows(index, 1)
                }
                Component.onCompleted: {
                    console.log("New ListItem created. Task title: " + model.title + " -- Priority: " + model.priority);
                }
            }
        }
    }
}
